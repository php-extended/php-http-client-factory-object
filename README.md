# php-extended/php-http-client-factory-object

This library is an implementation of the php-extended/php-http-client-factory-interface library.

![coverage](https://gitlab.com/php-extended/php-http-client-factory-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-http-client-factory-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-http-client-factory-object ^8`


## Basic Usage

This library provides a configurable factory to be able to build a full featured
processor very easily. Just do :

```php

use PhpExtended\HttpClient\ClientFactory;

$factory = new ClientFactory();
$client = $factory->createClient();
// $client instanceof \PhpExtended\HttpClient\ClientInterface

```

You may use the clientFactory methods to push configuration objects
which gathers all the settings for the underlying decorated client(s).


## License

MIT (See [license file](LICENSE)).
