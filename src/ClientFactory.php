<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-factory-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use PhpExtended\Blocklist\BlocklistCatalog;
use PhpExtended\Blocklist\BlocklistInterface;
use PhpExtended\Certificate\CertificateProviderInterface;
use PhpExtended\Certificate\MozillaCertificateProvider;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\ResponseFactory;
use PhpExtended\HttpMessage\StreamFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\MimeType\ApacheMimeTypeProvider;
use PhpExtended\MimeType\IanaMimeTypeProvider;
use PhpExtended\MimeType\MimeTypeProvider;
use PhpExtended\MimeType\MimeTypeProviderInterface;
use PhpExtended\Tld\MozillaTopLevelDomainProvider;
use PhpExtended\Tld\TopLevelDomainHierarchy;
use PhpExtended\Tld\TopLevelDomainHierarchyInterface;
use PhpExtended\Tld\TopLevelDomainProviderInterface;
use PhpExtended\UrlRedirecter\RedirecterFactory;
use PhpExtended\UrlRedirecter\RedirecterFactoryInterface;
use PhpExtended\UserAgent\UserAgentProviderInterface;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Psr\SimpleCache\CacheInterface;
use RuntimeException;

/**
 * ClientFactory class file.
 * 
 * This class creates clients with the given configuration.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class ClientFactory implements ClientFactoryInterface
{
	
	/**
	 * The request factory.
	 * 
	 * @var ?RequestFactoryInterface
	 */
	protected ?RequestFactoryInterface $_requestFactory = null;
	
	/**
	 * The uri factory.
	 * 
	 * @var ?UriFactoryInterface
	 */
	protected ?UriFactoryInterface $_uriFactory = null;
	
	/**
	 * The response factory.
	 * 
	 * @var ?ResponseFactoryInterface
	 */
	protected ?ResponseFactoryInterface $_responseFactory = null;
	
	/**
	 * The stream factory.
	 * 
	 * @var ?StreamFactoryInterface
	 */
	protected ?StreamFactoryInterface $_streamFactory = null;
	
	/**
	 * The logger.
	 * 
	 * @var ?LoggerInterface
	 */
	protected ?LoggerInterface $_logger = null;
	
	/**
	 * The simple cache.
	 * 
	 * @var ?CacheInterface
	 */
	protected ?CacheInterface $_simpleCache = null;
	
	/**
	 * The cache pool.
	 * 
	 * @var ?CacheItemPoolInterface
	 */
	protected ?CacheItemPoolInterface $_cachePool = null;
	
	/**
	 * The blocklist.
	 * 
	 * @var ?BlocklistInterface
	 */
	protected ?BlocklistInterface $_blocklist = null;
	
	/**
	 * The certificate provider.
	 * 
	 * @var ?CertificateProviderInterface
	 */
	protected ?CertificateProviderInterface $_certificateProvider = null;
	
	/**
	 * The mime type provider.
	 * 
	 * @var ?MimeTypeProviderInterface
	 */
	protected ?MimeTypeProviderInterface $_mimeTypeProvider = null;
	
	/**
	 * The top level domain provider.
	 * 
	 * @var ?TopLevelDomainProviderInterface
	 */
	protected ?TopLevelDomainProviderInterface $_tldProvider = null;
	
	/**
	 * The user agent provider.
	 * 
	 * @var ?UserAgentProviderInterface
	 */
	protected ?UserAgentProviderInterface $_userAgentProvider = null;
	
	/**
	 * The url redirecter factory.
	 * 
	 * @var ?RedirecterFactoryInterface
	 */
	protected ?RedirecterFactoryInterface $_urlRedirecterFactory = null;
	
	/**
	 * The client factory configuration.
	 * 
	 * @var ?ClientFactoryConfiguration
	 */
	protected ?ClientFactoryConfiguration $_config = null;
	
	/**
	 * The top level domain hierarchy.
	 *
	 * @var ?TopLevelDomainHierarchyInterface
	 */
	private ?TopLevelDomainHierarchyInterface $_tldHierarchy = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the request factory for this client factory.
	 * 
	 * @param RequestFactoryInterface $requestFactory
	 * @return ClientFactory
	 */
	public function setRequestFactory(RequestFactoryInterface $requestFactory) : ClientFactory
	{
		$this->_requestFactory = $requestFactory;
		
		return $this;
	}
	
	/**
	 * Gets the request factory for this client factory.
	 * 
	 * @return RequestFactoryInterface
	 */
	public function getRequestFactory() : RequestFactoryInterface
	{
		if(null === $this->_requestFactory)
		{
			$this->_requestFactory = new RequestFactory();
		}
		
		return $this->_requestFactory;
	}
	
	/**
	 * Sets the uri factory for this client factory.
	 * 
	 * @param UriFactoryInterface $uriFactory
	 * @return ClientFactory
	 */
	public function setUriFactory(UriFactoryInterface $uriFactory) : ClientFactory
	{
		$this->_uriFactory = $uriFactory;
		
		return $this;
	}
	
	/**
	 * Gets the uri factory for this client factory.
	 * 
	 * @return UriFactoryInterface
	 */
	public function getUriFactory() : UriFactoryInterface
	{
		if(null === $this->_uriFactory)
		{
			$this->_uriFactory = new UriFactory();
		}
		
		return $this->_uriFactory;
	}
	
	/**
	 * Sets the response factory for this client factory.
	 * 
	 * @param ResponseFactoryInterface $responseFactory
	 * @return ClientFactory
	 */
	public function setResponseFactory(ResponseFactoryInterface $responseFactory) : ClientFactory
	{
		$this->_responseFactory = $responseFactory;
		
		return $this;
	}
	
	/**
	 * Gets the response factory for this client factory.
	 * 
	 * @return ResponseFactoryInterface
	 */
	public function getResponseFactory() : ResponseFactoryInterface
	{
		if(null === $this->_responseFactory)
		{
			$this->_responseFactory = new ResponseFactory();
		}
		
		return $this->_responseFactory;
	}
	
	/**
	 * Sets the stream factory for this client factory.
	 * 
	 * @param StreamFactoryInterface $streamFactory
	 * @return ClientFactory
	 */
	public function setStreamFactory(StreamFactoryInterface $streamFactory) : ClientFactory
	{
		$this->_streamFactory = $streamFactory;
		
		return $this;
	}
	
	/**
	 * Gets the stream factory for this client factory.
	 * 
	 * @return StreamFactoryInterface
	 */
	public function getStreamFactory() : StreamFactoryInterface
	{
		if(null === $this->_streamFactory)
		{
			$this->_streamFactory = new StreamFactory();
		}
		
		return $this->_streamFactory;
	}
	
	/**
	 * Sets the logger for this client factory.
	 * 
	 * @param LoggerInterface $logger
	 * @return ClientFactory
	 */
	public function setLogger(LoggerInterface $logger) : ClientFactory
	{
		$this->_logger = $logger;
		
		return $this;
	}
	
	/**
	 * Gets the logger for this client factory, if any.
	 * 
	 * @return LoggerInterface
	 */
	public function getLogger() : LoggerInterface
	{
		if(null === $this->_logger)
		{
			$this->_logger = new NullLogger();
		}
		
		return $this->_logger;
	}
	
	/**
	 * Sets the simple cache for this client factory.
	 * 
	 * @param CacheInterface $cache
	 * @return ClientFactory
	 */
	public function setCache(CacheInterface $cache) : ClientFactory
	{
		$this->_simpleCache = $cache;
		
		return $this;
	}
	
	/**
	 * Gets the logger for this client factory, if any.
	 * 
	 * @return ?CacheInterface
	 */
	public function getCache() : ?CacheInterface
	{
		return $this->_simpleCache;
	}
	
	/**
	 * Sets the cache item pool for this client factory.
	 * 
	 * @param CacheItemPoolInterface $pool
	 * @return ClientFactory
	 */
	public function setCacheItemPool(CacheItemPoolInterface $pool) : ClientFactory
	{
		$this->_cachePool = $pool;
		
		return $this;
	}
	
	/**
	 * Gets the cache item pool for this client factory, if any.
	 * 
	 * @return ?CacheItemPoolInterface
	 */
	public function getCacheItemPool() : ?CacheItemPoolInterface
	{
		return $this->_cachePool;
	}
	
	/**
	 * Sets the blocklist for this client factory.
	 * 
	 * @param BlocklistInterface $blocklist
	 * @return ClientFactory
	 */
	public function setBlocklist(BlocklistInterface $blocklist) : ClientFactory
	{
		$this->_blocklist = $blocklist;
		
		return $this;
	}
	
	/**
	 * Gets the blocklist for this client factory.
	 * 
	 * @return BlocklistInterface
	 */
	public function getBlocklist() : BlocklistInterface
	{
		if(null === $this->_blocklist)
		{
			$this->_blocklist = new BlocklistCatalog($this->getTopLevelDomainHierarchy());
		}
		
		return $this->_blocklist;
	}
	
	/**
	 * Sets the certificate provider for this client factory.
	 * 
	 * @param CertificateProviderInterface $provider
	 * @return ClientFactory
	 */
	public function setCertificateProvider(CertificateProviderInterface $provider) : ClientFactory
	{
		$this->_certificateProvider = $provider;
		
		return $this;
	}
	
	/**
	 * Gets the certificate provider for this client factory.
	 * 
	 * @return CertificateProviderInterface
	 */
	public function getCertificateProvider() : CertificateProviderInterface
	{
		if(null === $this->_certificateProvider)
		{
			$this->_certificateProvider = new MozillaCertificateProvider();
		}
		
		return $this->_certificateProvider;
	}
	
	/**
	 * Sets the mime type provider for this client factory.
	 * 
	 * @param MimeTypeProviderInterface $provider
	 * @return ClientFactory
	 */
	public function setMimeTypeProvider(MimeTypeProviderInterface $provider) : ClientFactory
	{
		$this->_mimeTypeProvider = $provider;
		
		return $this;
	}
	
	/**
	 * Gets the mime type provider for this client factory.
	 * 
	 * @return MimeTypeProviderInterface
	 */
	public function getMimeTypeProvider() : MimeTypeProviderInterface
	{
		if(null === $this->_mimeTypeProvider)
		{
			$provider = new MimeTypeProvider([], []);
			
			try
			{
				$provider->absorbProvider(new ApacheMimeTypeProvider());
			}
			catch(RuntimeException $e)
			{
				$this->getLogger()->error($e->getMessage()."\n\n".$e->getTraceAsString());
			}
			
			try
			{
				$provider->absorbProvider(new IanaMimeTypeProvider());
			}
			catch(RuntimeException $e)
			{
				$this->getLogger()->error($e->getMessage()."\n\n".$e->getTraceAsString());
			}
			
			$this->_mimeTypeProvider = $provider;
		}
		
		return $this->_mimeTypeProvider;
	}
	
	/**
	 * Sets the top level domain provider for this client factory.
	 * 
	 * @param TopLevelDomainProviderInterface $provider
	 * @return ClientFactory
	 */
	public function setTopLevelDomainProvider(TopLevelDomainProviderInterface $provider) : ClientFactory
	{
		$this->_tldProvider = $provider;
		
		return $this;
	}
	
	/**
	 * Gets the top level domain provider for this client factory.
	 * 
	 * @return TopLevelDomainProviderInterface
	 */
	public function getTopLevelDomainProvider() : TopLevelDomainProviderInterface
	{
		if(null === $this->_tldProvider)
		{
			$this->_tldProvider = new MozillaTopLevelDomainProvider();
		}
		
		return $this->_tldProvider;
	}
	
	/**
	 * Gets the top level domain hierarchy for this client factory.
	 * 
	 * @return TopLevelDomainHierarchyInterface
	 */
	public function getTopLevelDomainHierarchy() : TopLevelDomainHierarchyInterface
	{
		if(null === $this->_tldHierarchy)
		{
			try
			{
				$this->_tldHierarchy = $this->getTopLevelDomainProvider()->getHierarchy();
			}
			catch(RuntimeException $e)
			{
				$this->getLogger()->error($e->getMessage()."\n\n".$e->getTraceAsString());
				$this->_tldHierarchy = new TopLevelDomainHierarchy();
			}
		}
		
		return $this->_tldHierarchy;
	}
	
	/**
	 * Sets the user agent provider for the this factory.
	 * 
	 * @param UserAgentProviderInterface $provider
	 * @return ClientFactory
	 */
	public function setUserAgentProvider(UserAgentProviderInterface $provider) : ClientFactory
	{
		$this->_userAgentProvider = $provider;
		
		return $this;
	}
	
	/**
	 * Gets the user agent provider for this client factory, if any.
	 * 
	 * @return ?UserAgentProviderInterface
	 */
	public function getUserAgentProvider() : ?UserAgentProviderInterface
	{
		return $this->_userAgentProvider;
	}
	
	/**
	 * Sets the url redirecter factory for this client.
	 * 
	 * @param RedirecterFactoryInterface $factory
	 * @return ClientFactory
	 */
	public function setUrlRedirecterFactory(RedirecterFactoryInterface $factory) : ClientFactory
	{
		$this->_urlRedirecterFactory = $factory;
		
		return $this;
	}
	
	/**
	 * Sets the client factory configuration for this client factory.
	 * 
	 * @param ClientFactoryConfiguration $config
	 * @return ClientFactory
	 */
	public function setClientFactoryConfiguration(ClientFactoryConfiguration $config) : ClientFactory
	{
		$this->_config = $config;
		
		return $this;
	}
	
	/**
	 * Gets the client factory configuration for this client factory.
	 * 
	 * @return ClientFactoryConfiguration
	 */
	public function getConfiguration() : ClientFactoryConfiguration
	{
		if(null === $this->_config)
		{
			$this->_config = new ClientFactoryConfiguration();
		}
		
		return $this->_config;
	}
	
	/**
	 * Creates a new client with the given configuration.
	 * 
	 * @return ClientInterface
	 */
	public function createClient() : ClientInterface
	{
		$client = new NativeClient($this->getResponseFactory(), $this->getStreamFactory(), new NativeOptionsFactory(), $this->getConfiguration()->getNativeOptions(), $this->getCertificateProvider());
		
		$client = $this->decorateLoggerClient($client);
		$client = $this->decorateRetryClient($client);
		$client = $this->decorateCacheItemClient($client);
		$client = $this->decorateSimpleCacheClient($client);
		
		if($this->getConfiguration()->isPreferredCurl() && \extension_loaded('curl'))
		{
			// TODO add response and stream factories
			$curlClient = new CurlClient($this->getCertificateProvider(), $this->getConfiguration()->getCurlOptions());
			
			$curlClient = $this->decorateLoggerClient($curlClient);
			$curlClient = $this->decorateRetryClient($curlClient);
			$curlClient = $this->decorateCacheItemClient($curlClient);
			$curlClient = $this->decorateSimpleCacheClient($curlClient);
			$client = new TryCatchClient($client, $curlClient);
		}
		
		$client = $this->decorateCookieClient($client);
		$client = $this->decodateZipClient($client);
		$client = $this->decorateGzipClient($client);
		$client = $this->decorateUserAgentClient($client);
		$client = $this->decorateUpdateInsecureClient($client);
		$client = $this->decorateReferrerClient($client);
		$client = $this->decorateOriginClient($client);
		$client = $this->decorateHostClient($client);
		$client = $this->decorateDoNotTrackClient($client);
		$client = $this->decorateDateClient($client);
		$client = $this->decorateConnectionClient($client);
		$client = $this->decorateAcceptLanguageClient($client);
		$client = $this->decorateAcceptCharsetClient($client);
		$client = $this->decorateAcceptClient($client);
		$client = $this->decorateRedirecterClient($client);
		
		return $this->decorateBlocklistClient($client);
	}
	
	/**
	 * Gets the logger client decorator.
	 * 
	 * @param ClientInterface $client
	 * @return ClientInterface
	 */
	protected function decorateLoggerClient(ClientInterface $client) : ClientInterface
	{
		if(null !== $this->_logger)
		{
			return new LoggerClient($client, $this->getLogger());
		}
		
		return $client;
	}
	
	/**
	 * Gets the cache item client decorator.
	 * 
	 * @param ClientInterface $client
	 * @return ClientInterface
	 */
	protected function decorateCacheItemClient(ClientInterface $client) : ClientInterface
	{
		if(null !== $this->_cachePool)
		{
			return new CacheItemPoolClient($client, $this->_cachePool, $this->getResponseFactory(), $this->getStreamFactory(), $this->getConfiguration()->getCacheItemPoolOptions());
		}
		
		return $client;
	}
	
	/**
	 * Gets the simple cache client decorator.
	 * 
	 * @param ClientInterface $client
	 * @return ClientInterface
	 */
	protected function decorateSimpleCacheClient(ClientInterface $client) : ClientInterface
	{
		if(null !== $this->_simpleCache)
		{
			return new SimpleCacheClient($client, $this->_simpleCache, $this->getResponseFactory(), $this->getStreamFactory(), $this->getConfiguration()->getSimpleCacheOptions());
		}
		
		return $client;
	}
	
	/**
	 * Gets the retry client decorator.
	 * 
	 * @param ClientInterface $client
	 * @return ClientInterface
	 */
	protected function decorateRetryClient(ClientInterface $client) : ClientInterface
	{
		if($this->getConfiguration()->isEnabledRetry())
		{
			return new RetryClient($client, $this->getConfiguration()->getRetryOptions());
		}
		
		return $client;
	}
	
	/**
	 * Gets the cookie client decorator.
	 * 
	 * @param ClientInterface $client
	 * @return ClientInterface
	 */
	protected function decorateCookieClient(ClientInterface $client) : ClientInterface
	{
		if($this->getConfiguration()->isEnabledCookiebag())
		{
			return new CookieBagClient($client, $this->getTopLevelDomainHierarchy(), $this->getConfiguration()->getCookiebagOptions());
		}
		
		return $client;
	}
	
	/**
	 * Gets the gzip client decorator.
	 * 
	 * @param ClientInterface $client
	 * @return ClientInterface
	 */
	protected function decorateGzipClient(ClientInterface $client) : ClientInterface
	{
		if($this->getConfiguration()->isEnabledGzip())
		{
			return new GzipClient($client, $this->getStreamFactory());
		}
		
		return $client;
	}
	
	/**
	 * Gets the zip client decorator.
	 * 
	 * @param ClientInterface $client
	 * @return ClientInterface
	 */
	protected function decodateZipClient(ClientInterface $client) : ClientInterface
	{
		if($this->getConfiguration()->isEnabledZip())
		{
			return new ZipClient($client, $this->getStreamFactory());
		}
		
		return $client;
	}
	
	/**
	 * Gets the user agent client decorator.
	 * 
	 * @param ClientInterface $client
	 * @return ClientInterface
	 */
	protected function decorateUserAgentClient(ClientInterface $client) : ClientInterface
	{
		return new UserAgentClient($client, $this->_userAgentProvider);
	}
	
	/**
	 * Gets the upgrade insecure request client decorator.
	 * 
	 * @param ClientInterface $client
	 * @return ClientInterface
	 */
	protected function decorateUpdateInsecureClient(ClientInterface $client) : ClientInterface
	{
		if($this->getConfiguration()->isEnabledUpgradeInsecureRequests())
		{
			return new UpgradeInsecureRequestsClient($client);
		}
		
		return $client;
	}
	
	/**
	 * Gets the referrer client decorator.
	 * 
	 * @param ClientInterface $client
	 * @return ClientInterface
	 */
	protected function decorateReferrerClient(ClientInterface $client) : ClientInterface
	{
		if($this->getConfiguration()->isEnabledReferrer())
		{
			return new ReferrerClient($client, $this->getConfiguration()->getReferrerOptions());
		}
		
		return $client;
	}
	
	/**
	 * Gets the origin client decorator.
	 * 
	 * @param ClientInterface $client
	 * @return ClientInterface
	 */
	protected function decorateOriginClient(ClientInterface $client) : ClientInterface
	{
		if($this->getConfiguration()->isEnabledOrigin())
		{
			return new OriginClient($client);
		}
		
		return $client;
	}
	
	/**
	 * Gets the host client decorator.
	 * 
	 * @param ClientInterface $client
	 * @return ClientInterface
	 */
	protected function decorateHostClient(ClientInterface $client) : ClientInterface
	{
		if($this->getConfiguration()->isEnabledHost())
		{
			return new HostClient($client);
		}
		
		return $client;
	}
	
	/**
	 * Gets the do not track client decorator.
	 * 
	 * @param ClientInterface $client
	 * @return ClientInterface
	 */
	protected function decorateDoNotTrackClient(ClientInterface $client) : ClientInterface
	{
		if($this->getConfiguration()->isEnabledDnt())
		{
			return new DoNotTrackClient($client);
		}
		
		return $client;
	}
	
	/**
	 * Gets the date client decorator.
	 * 
	 * @param ClientInterface $client
	 * @return ClientInterface
	 */
	protected function decorateDateClient(ClientInterface $client) : ClientInterface
	{
		if($this->getConfiguration()->isEnabledDate())
		{
			return new DateClient($client);
		}
		
		return $client;
	}
	
	/**
	 * Gets the connection client decorator.
	 * 
	 * @param ClientInterface $client
	 * @return ClientInterface
	 */
	protected function decorateConnectionClient(ClientInterface $client) : ClientInterface
	{
		if($this->getConfiguration()->isEnabledConnection())
		{
			return new ConnectionClient($client, $this->getConfiguration()->getConnectionOptions());
		}
		
		return $client;
	}
	
	/**
	 * Gets the accept language client decorator.
	 * 
	 * @param ClientInterface $client
	 * @return ClientInterface
	 */
	protected function decorateAcceptLanguageClient(ClientInterface $client) : ClientInterface
	{
		if($this->getConfiguration()->isEnabledAcceptLanguage())
		{
			return new AcceptLanguageClient($client, $this->getConfiguration()->getAcceptLanguageOptions());
		}
		
		return $client;
	}
	
	/**
	 * Gets the accept charset client decorator.
	 * 
	 * @param ClientInterface $client
	 * @return ClientInterface
	 */
	protected function decorateAcceptCharsetClient(ClientInterface $client) : ClientInterface
	{
		if($this->getConfiguration()->isEnabledAcceptCharset())
		{
			return new AcceptCharsetClient($client, $this->getConfiguration()->getAcceptCharsetOptions());
		}
		
		return $client;
	}
	
	/**
	 * Gets the accept client decorator.
	 * 
	 * @param ClientInterface $client
	 * @return ClientInterface
	 */
	protected function decorateAcceptClient(ClientInterface $client) : ClientInterface
	{
		if($this->getConfiguration()->isEnabledAccept())
		{
			return new AcceptClient($client, $this->getMimeTypeProvider());
		}
		
		return $client;
	}
	
	/**
	 * Gets the redirecter client decorator.
	 * 
	 * @param ClientInterface $client
	 * @return ClientInterface
	 */
	protected function decorateRedirecterClient(ClientInterface $client) : ClientInterface
	{
		if($this->getConfiguration()->isEnabledRedirecter())
		{
			$redirecterFactory = $this->_urlRedirecterFactory;
			if(null === $redirecterFactory)
			{
				$redirecterFactory = new RedirecterFactory($client, $this->getRequestFactory(), $this->getUriFactory());
			}
			
			$redirecter = $redirecterFactory->createRedirecter($this->getConfiguration()->getRedirecterFactoryConfiguration());
			
			return new RedirecterClient($client, $redirecter);
		}
		
		return $client;
	}
	
	/**
	 * Gets the blocklist client decorator.
	 * 
	 * @param ClientInterface $client
	 * @return ClientInterface
	 */
	protected function decorateBlocklistClient(ClientInterface $client) : ClientInterface
	{
		if($this->getConfiguration()->isEnabledBlocklist())
		{
			return new BlocklistClient($client, $this->getResponseFactory(), $this->getBlocklist(), $this->getConfiguration()->getBlocklistOptions());
		}
		
		return $client;
	}
	
}
