<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-factory-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use PhpExtended\UrlRedirecter\RedirecterFactoryConfiguration;
use PhpExtended\UrlRedirecter\RedirecterFactoryConfigurationInterface;
use Stringable;

/**
 * ClientFactoryConfiguration class file.
 * 
 * This class represents the configuration for the various clients.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.ExcessivePublicCount")
 * @SuppressWarnings("PHPMD.TooManyFields")
 * @SuppressWarnings("PHPMD.TooManyMethods")
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 */
class ClientFactoryConfiguration implements Stringable
{
	
	/**
	 * Whether to enable the accept client. Defaults to true.
	 * 
	 * @var boolean
	 */
	protected bool $_enableAccept = true;
	
	/**
	 * Whether to enable the accept charset client. Defaults to true.
	 * 
	 * @var boolean
	 */
	protected bool $_enableAcceptCharset = true;
	
	/**
	 * The options for the accept charset client.
	 * 
	 * @var ?AcceptCharsetConfiguration
	 */
	protected ?AcceptCharsetConfiguration $_acceptCharsetOptions = null;
	
	/**
	 * Whether to enable the accept language client. Defaults to true.
	 * 
	 * @var boolean
	 */
	protected bool $_enableAcceptLanguage = true;
	
	/**
	 * The options for the accept language client.
	 * 
	 * @var ?AcceptLanguageConfiguration
	 */
	protected ?AcceptLanguageConfiguration $_acceptLanguageOptions = null;
	
	/**
	 * Whether to enable the connection client. Defaults to true.
	 * 
	 * @var boolean
	 */
	protected bool $_enableConnection = true;
	
	/**
	 * The options for the blocklist client.
	 * 
	 * @var ?BlocklistConfiguration
	 */
	protected ?BlocklistConfiguration $_blocklistOptions = null;
	
	/**
	 * Whether to enabled the blocklist client. Defaults to true.
	 * 
	 * @var boolean
	 */
	protected bool $_enableBlocklist = true;
	
	/**
	 * The options for the cache pool client.
	 * 
	 * @var ?CacheItemPoolConfiguration
	 */
	protected ?CacheItemPoolConfiguration $_cachePoolOptions = null;
	
	/**
	 * Whether to enable the cookiebag client. Defaults to true.
	 * 
	 * @var boolean
	 */
	protected bool $_enableCookiebag = true;
	
	/**
	 * The options for the cookie bag client.
	 * 
	 * @var ?CookieBagConfiguration
	 */
	protected ?CookieBagConfiguration $_cookiebagOptions = null;
	
	/**
	 * The options for the connection client.
	 *
	 * @var ?ConnectionConfiguration
	 */
	protected ?ConnectionConfiguration $_connectionOptions = null;
	
	/**
	 * Whether to prefer curl in case the extension is installed. Defaults to
	 * true.
	 * 
	 * @var boolean
	 */
	protected bool $_preferCurl = true;
	
	/**
	 * The options for curl.
	 * 
	 * @var ?CurlOptions
	 */
	protected ?CurlOptions $_curlOptions = null;
	
	/**
	 * Whether to enable the date client. Defaults to true.
	 * 
	 * @var boolean
	 */
	protected bool $_enableDate = true;
	
	/**
	 * Whether to enable the dnt (do-not-track) client. Defaults to true.
	 * 
	 * @var boolean
	 */
	protected bool $_enableDnt = true;
	
	/**
	 * Whether to enable the gzip compression client. Defaults to true.
	 * 
	 * @var boolean
	 */
	protected bool $_enableGzip = true;
	
	/**
	 * Whether to enable the host client. Defaults to true.
	 * 
	 * @var boolean
	 */
	protected bool $_enableHost = true;
	
	/**
	 * The options for the native client.
	 * 
	 * @var ?NativeOptions
	 */
	protected ?NativeOptions $_nativeOptions = null;
	
	/**
	 * Whether to enable the origin client. Defaults to true.
	 * 
	 * @var boolean
	 */
	protected bool $_enableOrigin = true;
	
	/**
	 * The options for the simple cache client.
	 * 
	 * @var ?SimpleCacheConfiguration
	 */
	protected ?SimpleCacheConfiguration $_simpleCacheOptions = null;
	
	/**
	 * Whether to enable the redirecter client. Defaults to true.
	 * 
	 * @var boolean
	 */
	protected bool $_enableRedirecter = true;
	
	/**
	 * The redirecter factory configuration.
	 * 
	 * @var ?RedirecterFactoryConfigurationInterface
	 */
	protected ?RedirecterFactoryConfigurationInterface $_redirecterConfiguration = null;
	
	/**
	 * Whether to enable the referrer client. Defaults to true.
	 * 
	 * @var boolean
	 */
	protected bool $_enableReferrer = true;
	
	/**
	 * The referrer configuration.
	 * 
	 * @var ?ReferrerConfiguration
	 */
	protected ?ReferrerConfiguration $_referrerOptions = null;
	
	/**
	 * Whether to enable the retry client. Defaults to true.
	 * 
	 * @var boolean
	 */
	protected bool $_enableRetry = true;
	
	/**
	 * The retry configuration.
	 * 
	 * @var ?RetryConfiguration
	 */
	protected ?RetryConfiguration $_retryOptions = null;
	
	/**
	 * Whether to enable the uir (upgrade-insecure-requests) client. Defaults
	 * to true.
	 * 
	 * @var boolean
	 */
	protected bool $_enableUpgradeInsecureRequests = true;
	
	/**
	 * Whether to enable the zip compression client. Defaults to true.
	 *
	 * @var boolean
	 */
	protected bool $_enableZip = true;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Enables the accept client.
	 */
	public function enableAccept() : void
	{
		$this->_enableAccept = true;
	}
	
	/**
	 * Disables the accept client.
	 */
	public function disableAccept() : void
	{
		$this->_enableAccept = false;
	}
	
	/**
	 * Gets whether the accept client is enabled.
	 * 
	 * @return bool
	 */
	public function isEnabledAccept() : bool
	{
		return $this->_enableAccept;
	}
	
	/**
	 * Enables the accept charset client.
	 */
	public function enableAcceptCharset() : void
	{
		$this->_enableAcceptCharset = true;
	}
	
	/**
	 * Disables the accept charset client.
	 */
	public function disableAcceptCharset() : void
	{
		$this->_enableAcceptCharset = false;
	}
	
	/**
	 * Gets whether the accept charset client is enabled.
	 * 
	 * @return boolean
	 */
	public function isEnabledAcceptCharset() : bool
	{
		return $this->_enableAcceptCharset;
	}
	
	/**
	 * Gets the accept charset configuration options.
	 * 
	 * @return AcceptCharsetConfiguration
	 */
	public function getAcceptCharsetOptions() : AcceptCharsetConfiguration
	{
		if(null === $this->_acceptCharsetOptions)
		{
			$this->_acceptCharsetOptions = new AcceptCharsetConfiguration();
		}
		
		return $this->_acceptCharsetOptions;
	}
	
	/**
	 * Enables the accept language client.
	 */
	public function enableAcceptLanguage() : void
	{
		$this->_enableAcceptLanguage = true;
	}
	
	/**
	 * Disables the accept language client.
	 */
	public function disableAcceptLanguage() : void
	{
		$this->_enableAcceptLanguage = false;
	}
	
	/**
	 * Gets whether the accept language client is enabled.
	 * 
	 * @return boolean
	 */
	public function isEnabledAcceptLanguage() : bool
	{
		return $this->_enableAcceptLanguage;
	}
	
	/**
	 * Gets the accept language configuration options.
	 * 
	 * @return AcceptLanguageConfiguration
	 */
	public function getAcceptLanguageOptions() : AcceptLanguageConfiguration
	{
		if(null === $this->_acceptLanguageOptions)
		{
			$this->_acceptLanguageOptions = new AcceptLanguageConfiguration();
		}
		
		return $this->_acceptLanguageOptions;
	}
	
	/**
	 * Enables the blocklist client.
	 */
	public function enableBlocklist() : void
	{
		$this->_enableBlocklist = true;
	}
	
	/**
	 * Disables the blocklist client.
	 */
	public function disableBlocklist() : void
	{
		$this->_enableBlocklist = false;
	}
	
	/**
	 * Gets whether the blocklist client is enabled.
	 * 
	 * @return boolean
	 */
	public function isEnabledBlocklist() : bool
	{
		return $this->_enableBlocklist;
	}
	
	/**
	 * Gets the blocklist configuration options.
	 * 
	 * @return BlocklistConfiguration
	 */
	public function getBlocklistOptions() : BlocklistConfiguration
	{
		if(null === $this->_blocklistOptions)
		{
			$this->_blocklistOptions = new BlocklistConfiguration();
		}
		
		return $this->_blocklistOptions;
	}
	
	/**
	 * Enables the connection client.
	 */
	public function enableConnection() : void
	{
		$this->_enableConnection = true;
	}
	
	/**
	 * Disables the connection client.
	 */
	public function disableConnection() : void
	{
		$this->_enableConnection = false;
	}
	
	/**
	 * Gets whether the connection client is enabled.
	 * 
	 * @return boolean
	 */
	public function isEnabledConnection() : bool
	{
		return $this->_enableConnection;
	}
	
	/**
	 * Gets the options for the connection client.
	 * 
	 * @return ConnectionConfiguration
	 */
	public function getConnectionOptions() : ConnectionConfiguration
	{
		if(null === $this->_connectionOptions)
		{
			$this->_connectionOptions = new ConnectionConfiguration();
		}
		
		return $this->_connectionOptions;
	}
	
	/**
	 * Gets the options for the cache item pool client.
	 * 
	 * @return CacheItemPoolConfiguration
	 */
	public function getCacheItemPoolOptions() : CacheItemPoolConfiguration
	{
		if(null === $this->_cachePoolOptions)
		{
			$this->_cachePoolOptions = new CacheItemPoolConfiguration();
		}
		
		return $this->_cachePoolOptions;
	}
	
	/**
	 * Enables the cookie bag client.
	 */
	public function enableCookiebag() : void
	{
		$this->_enableCookiebag = true;
	}
	
	/**
	 * Disables the cookie bag client.
	 */
	public function disableCookiebag() : void
	{
		$this->_enableCookiebag = false;
	}
	
	/**
	 * Gets whether the cookie bag client is enabled.
	 * 
	 * @return boolean
	 */
	public function isEnabledCookiebag() : bool
	{
		return $this->_enableCookiebag;
	}
	
	/**
	 * Gets the options for the cookie bag client.
	 * 
	 * @return CookieBagConfiguration
	 */
	public function getCookiebagOptions() : CookieBagConfiguration
	{
		if(null === $this->_cookiebagOptions)
		{
			$this->_cookiebagOptions = new CookieBagConfiguration();
		}
		
		return $this->_cookiebagOptions;
	}
	
	/**
	 * Enables the preference for the curl client.
	 */
	public function enablePreferCurl() : void
	{
		$this->_preferCurl = true;
	}
	
	/**
	 * Disables the preference for the curl client. 
	 */
	public function disablePreferCurl() : void
	{
		$this->_preferCurl = false;
	}
	
	/**
	 * Gets whether the curl client is preferred over the native one.
	 * 
	 * @return boolean
	 */
	public function isPreferredCurl() : bool
	{
		return $this->_preferCurl;
	}
	
	/**
	 * Gets the options for the curl client.
	 * 
	 * @return CurlOptions
	 */
	public function getCurlOptions() : CurlOptions
	{
		if(null === $this->_curlOptions)
		{
			$this->_curlOptions = new CurlOptions();
		}
		
		return $this->_curlOptions;
	}
	
	/**
	 * Enables the date client.
	 */
	public function enableDate() : void
	{
		$this->_enableDate = true;
	}
	
	/**
	 * Disables the date client.
	 */
	public function disableDate() : void
	{
		$this->_enableDate = false;
	}
	
	/**
	 * Gets whether the date client is enabled.
	 * 
	 * @return boolean
	 */
	public function isEnabledDate() : bool
	{
		return $this->_enableDate;
	}
	
	/**
	 * Enables the dnt client.
	 */
	public function enableDnt() : void
	{
		$this->_enableDnt = true;
	}
	
	/**
	 * Disables the dnt client.
	 */
	public function disableDnt() : void
	{
		$this->_enableDnt = false;
	}
	
	/**
	 * Gets whether the dnt client is enabled.
	 * 
	 * @return boolean
	 */
	public function isEnabledDnt() : bool
	{
		return $this->_enableDnt;
	}
	
	/**
	 * Enables the gzip compression client.
	 */
	public function enableGzip() : void
	{
		$this->_enableGzip = true;
	}
	
	/**
	 * Disables the gzip compression client.
	 */
	public function disableGzip() : void
	{
		$this->_enableGzip = false;
	}
	
	/**
	 * Gets whether the gzip compression client is enabled.
	 * 
	 * @return boolean
	 */
	public function isEnabledGzip() : bool
	{
		return $this->_enableGzip;
	}
	
	/**
	 * Enables the host client.
	 */
	public function enableHost() : void
	{
		$this->_enableHost = true;
	}
	
	/**
	 * Disables the host client.
	 */
	public function disableHost() : void
	{
		$this->_enableHost = false;
	}
	
	/**
	 * Gets whether the host client is enabled.
	 * 
	 * @return boolean
	 */
	public function isEnabledHost() : bool
	{
		return $this->_enableHost;
	}
	
	/**
	 * Gets the options for the native client.
	 * 
	 * @return NativeOptions
	 */
	public function getNativeOptions() : NativeOptions
	{
		if(null === $this->_nativeOptions)
		{
			$this->_nativeOptions = new NativeOptions();
		}
		
		return $this->_nativeOptions;
	}
	
	/**
	 * Enables the origin client.
	 */
	public function enableOrigin() : void
	{
		$this->_enableOrigin = true;
	}
	
	/**
	 * Disables the origin client.
	 */
	public function disableOrigin() : void
	{
		$this->_enableOrigin = false;
	}
	
	/**
	 * Gets whether the origin client is enabled.
	 * 
	 * @return boolean
	 */
	public function isEnabledOrigin() : bool
	{
		return $this->_enableOrigin;
	}
	
	/**
	 * Gets the options for the simple cache client.
	 * 
	 * @return SimpleCacheConfiguration
	 */
	public function getSimpleCacheOptions() : SimpleCacheConfiguration
	{
		if(null === $this->_simpleCacheOptions)
		{
			$this->_simpleCacheOptions = new SimpleCacheConfiguration();
		}
		
		return $this->_simpleCacheOptions;
	}
	
	/**
	 * Enables the redirecter client.
	 */
	public function enableRedirecter() : void
	{
		$this->_enableRedirecter = true;
	}
	
	/**
	 * Disables the redirecter client.
	 */
	public function disableRedirecter() : void
	{
		$this->_enableRedirecter = false;
	}
	
	/**
	 * Gets whether the redirecter client is enabled.
	 * 
	 * @return boolean
	 */
	public function isEnabledRedirecter() : bool
	{
		return $this->_enableRedirecter;
	}
	
	/**
	 * Gets the redirecter factory configuration.
	 * 
	 * @return RedirecterFactoryConfigurationInterface
	 */
	public function getRedirecterFactoryConfiguration() : RedirecterFactoryConfigurationInterface
	{
		if(null === $this->_redirecterConfiguration)
		{
			$this->_redirecterConfiguration = new RedirecterFactoryConfiguration();
		}
		
		return $this->_redirecterConfiguration;
	}
	
	/**
	 * Enables the referrer client.
	 */
	public function enableReferrer() : void
	{
		$this->_enableReferrer = true;
	}
	
	/**
	 * Disables the referrer client.
	 */
	public function disableReferrer() : void
	{
		$this->_enableReferrer = false;
	}
	
	/**
	 * Gets whether the referrer client is enabled.
	 * 
	 * @return boolean
	 */
	public function isEnabledReferrer() : bool
	{
		return $this->_enableReferrer;
	}
	
	/**
	 * Gets the referrer configuration.
	 * 
	 * @return ReferrerConfiguration
	 */
	public function getReferrerOptions() : ReferrerConfiguration
	{
		if(null === $this->_referrerOptions)
		{
			$this->_referrerOptions = new ReferrerConfiguration();
		}
		
		return $this->_referrerOptions;
	}
	
	/**
	 * Enables the retry client.
	 */
	public function enableRetry() : void
	{
		$this->_enableRetry = true;
	}
	
	/**
	 * Disables the retry client.
	 */
	public function disableRetry() : void
	{
		$this->_enableRetry = false;
	}
	
	/**
	 * Gets whether the retry client is enabled.
	 * 
	 * @return boolean
	 */
	public function isEnabledRetry() : bool
	{
		return $this->_enableRetry;
	}
	
	/**
	 * Gets the retry configuration.
	 * 
	 * @return RetryConfiguration
	 */
	public function getRetryOptions() : RetryConfiguration
	{
		if(null === $this->_retryOptions)
		{
			$this->_retryOptions = new RetryConfiguration();
		}
		
		return $this->_retryOptions;
	}
	
	/**
	 * Enables the uir client.
	 */
	public function enableUpgrageInsecureRequests() : void
	{
		$this->_enableUpgradeInsecureRequests = true;
	}
	
	/**
	 * Disables the uir client.
	 */
	public function disableUpgradeInsecureRequests() : void
	{
		$this->_enableUpgradeInsecureRequests = false;
	}
	
	/**
	 * Gets whether the uir client is enabled.
	 * 
	 * @return boolean
	 */
	public function isEnabledUpgradeInsecureRequests() : bool
	{
		return $this->_enableUpgradeInsecureRequests;
	}
	
	/**
	 * Enables the zip compression client.
	 */
	public function enableZip() : void
	{
		$this->_enableZip = true;
	}
	
	/**
	 * Disables the zip compression client.
	 */
	public function disableZip() : void
	{
		$this->_enableZip = false;
	}
	
	/**
	 * Gets whether the zip compression client is enabled.
	 *
	 * @return boolean
	 */
	public function isEnabledZip() : bool
	{
		return $this->_enableZip;
	}
	
}
