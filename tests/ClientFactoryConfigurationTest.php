<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-factory-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\AcceptCharsetConfiguration;
use PhpExtended\HttpClient\AcceptLanguageConfiguration;
use PhpExtended\HttpClient\BlocklistConfiguration;
use PhpExtended\HttpClient\CacheItemPoolConfiguration;
use PhpExtended\HttpClient\ClientFactoryConfiguration;
use PhpExtended\HttpClient\ConnectionConfiguration;
use PhpExtended\HttpClient\CookieBagConfiguration;
use PhpExtended\HttpClient\CurlOptions;
use PhpExtended\HttpClient\NativeOptions;
use PhpExtended\HttpClient\ReferrerConfiguration;
use PhpExtended\HttpClient\RetryConfiguration;
use PhpExtended\HttpClient\SimpleCacheConfiguration;
use PhpExtended\UrlRedirecter\RedirecterFactoryConfiguration;
use PHPUnit\Framework\TestCase;

/**
 * ClientFactoryConfigurationTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\ClientFactoryConfiguration
 *
 * @internal
 *
 * @small
 */
class ClientFactoryConfigurationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ClientFactoryConfiguration
	 */
	protected ClientFactoryConfiguration $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testEnableAccept() : void
	{
		$this->assertTrue($this->_object->isEnabledAccept());
		$this->_object->disableAccept();
		$this->assertFalse($this->_object->isEnabledAccept());
		$this->_object->enableAccept();
		$this->assertTrue($this->_object->isEnabledAccept());
	}
	
	public function testEnableAcceptCharset() : void
	{
		$this->assertTrue($this->_object->isEnabledAcceptCharset());
		$this->_object->disableAcceptCharset();
		$this->assertFalse($this->_object->isEnabledAcceptCharset());
		$this->_object->enableAcceptCharset();
		$this->assertTrue($this->_object->isEnabledAcceptCharset());
	}
	
	public function testGetAcceptCharsetOptions() : void
	{
		$this->assertInstanceOf(AcceptCharsetConfiguration::class, $this->_object->getAcceptCharsetOptions());
	}
	
	public function testEnableAcceptLanguage() : void
	{
		$this->assertTrue($this->_object->isEnabledAcceptLanguage());
		$this->_object->disableAcceptLanguage();
		$this->assertFalse($this->_object->isEnabledAcceptLanguage());
		$this->_object->enableAcceptLanguage();
		$this->assertTrue($this->_object->isEnabledAcceptLanguage());
	}
	
	public function testGetAcceptLanguageOptions() : void
	{
		$this->assertInstanceOf(AcceptLanguageConfiguration::class, $this->_object->getAcceptLanguageOptions());
	}
	
	public function testEnableBlocklist() : void
	{
		$this->assertTrue($this->_object->isEnabledBlocklist());
		$this->_object->disableBlocklist();
		$this->assertFalse($this->_object->isEnabledBlocklist());
		$this->_object->enableBlocklist();
		$this->assertTrue($this->_object->isEnabledBlocklist());
	}
	
	public function testGetBlocklistOptions() : void
	{
		$this->assertInstanceOf(BlocklistConfiguration::class, $this->_object->getBlocklistOptions());
	}
	
	public function testEnableConnection() : void
	{
		$this->assertTrue($this->_object->isEnabledConnection());
		$this->_object->disableConnection();
		$this->assertFalse($this->_object->isEnabledConnection());
		$this->_object->enableConnection();
		$this->assertTrue($this->_object->isEnabledConnection());
	}
	
	public function testGetConnectionOption() : void
	{
		$this->assertInstanceOf(ConnectionConfiguration::class, $this->_object->getConnectionOptions());
	}
	
	public function testGetCacheItemPoolOptions() : void
	{
		$this->assertInstanceOf(CacheItemPoolConfiguration::class, $this->_object->getCacheItemPoolOptions());
	}
	
	public function testEnableCookiebag() : void
	{
		$this->assertTrue($this->_object->isEnabledCookiebag());
		$this->_object->disableCookiebag();
		$this->assertFalse($this->_object->isEnabledCookiebag());
		$this->_object->enableCookiebag();
		$this->assertTrue($this->_object->isEnabledCookiebag());
	}
	
	public function testGetCookiebagOptions() : void
	{
		$this->assertInstanceOf(CookieBagConfiguration::class, $this->_object->getCookiebagOptions());
	}
	
	public function testEnablePreferCurl() : void
	{
		$this->assertTrue($this->_object->isPreferredCurl());
		$this->_object->disablePreferCurl();
		$this->assertFalse($this->_object->isPreferredCurl());
		$this->_object->enablePreferCurl();
		$this->assertTrue($this->_object->isPreferredCurl());
	}
	
	public function testGetCurlOptions() : void
	{
		$this->assertInstanceOf(CurlOptions::class, $this->_object->getCurlOptions());
	}
	
	public function testEnableDate() : void
	{
		$this->assertTrue($this->_object->isEnabledDate());
		$this->_object->disableDate();
		$this->assertFalse($this->_object->isEnabledDate());
		$this->_object->enableDate();
		$this->assertTrue($this->_object->isEnabledDate());
	}
	
	public function testEnableDnt() : void
	{
		$this->assertTrue($this->_object->isEnabledDnt());
		$this->_object->disableDnt();
		$this->assertFalse($this->_object->isEnabledDnt());
		$this->_object->enableDnt();
		$this->assertTrue($this->_object->isEnabledDnt());
	}
	
	public function testEnableGzip() : void
	{
		$this->assertTrue($this->_object->isEnabledGzip());
		$this->_object->disableGzip();
		$this->assertFalse($this->_object->isEnabledGzip());
		$this->_object->enableGzip();
		$this->assertTrue($this->_object->isEnabledGzip());
	}
	
	public function testEnableHost() : void
	{
		$this->assertTrue($this->_object->isEnabledHost());
		$this->_object->disableHost();
		$this->assertFalse($this->_object->isEnabledHost());
		$this->_object->enableHost();
		$this->assertTrue($this->_object->isEnabledHost());
	}
	
	public function testGetNativeOptions() : void
	{
		$this->assertInstanceOf(NativeOptions::class, $this->_object->getNativeOptions());
	}
	
	public function testEnableOrigin() : void
	{
		$this->assertTrue($this->_object->isEnabledOrigin());
		$this->_object->disableOrigin();
		$this->assertFalse($this->_object->isEnabledOrigin());
		$this->_object->enableOrigin();
		$this->assertTrue($this->_object->isEnabledOrigin());
	}
	
	public function testSimpleCacheOptions() : void
	{
		$this->assertInstanceOf(SimpleCacheConfiguration::class, $this->_object->getSimpleCacheOptions());
	}
	
	public function testEnableRedirecter() : void
	{
		$this->assertTrue($this->_object->isEnabledRedirecter());
		$this->_object->disableRedirecter();
		$this->assertFalse($this->_object->isEnabledRedirecter());
		$this->_object->enableRedirecter();
		$this->assertTrue($this->_object->isEnabledRedirecter());
	}
	
	public function testGetRedirecterFactoryConfiguration() : void
	{
		$this->assertInstanceOf(RedirecterFactoryConfiguration::class, $this->_object->getRedirecterFactoryConfiguration());
	}
	
	public function testEnableReferrer() : void
	{
		$this->assertTrue($this->_object->isEnabledReferrer());
		$this->_object->disableReferrer();
		$this->assertFalse($this->_object->isEnabledReferrer());
		$this->_object->enableReferrer();
		$this->assertTrue($this->_object->isEnabledReferrer());
	}
	
	public function testGetReferrerOptions() : void
	{
		$this->assertInstanceOf(ReferrerConfiguration::class, $this->_object->getReferrerOptions());
	}
	
	public function testEnableRetry() : void
	{
		$this->assertTrue($this->_object->isEnabledRetry());
		$this->_object->disableRetry();
		$this->assertFalse($this->_object->isEnabledRetry());
		$this->_object->enableRetry();
		$this->assertTrue($this->_object->isEnabledRetry());
	}
	
	public function testGetRetryOptions() : void
	{
		$this->assertInstanceOf(RetryConfiguration::class, $this->_object->getRetryOptions());
	}
	
	public function testEnableUpgradeInsecureRequest() : void
	{
		$this->assertTrue($this->_object->isEnabledUpgradeInsecureRequests());
		$this->_object->disableUpgradeInsecureRequests();
		$this->assertFalse($this->_object->isEnabledUpgradeInsecureRequests());
		$this->_object->enableUpgrageInsecureRequests();
		$this->assertTrue($this->_object->isEnabledUpgradeInsecureRequests());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ClientFactoryConfiguration();
	}
	
}
