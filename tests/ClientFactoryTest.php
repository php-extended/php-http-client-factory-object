<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-factory-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\ClientFactory;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;

/**
 * ClientFactoryTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\ClientFactory
 *
 * @internal
 *
 * @small
 */
class ClientFactoryTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ClientFactory
	 */
	protected ClientFactory $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetDefaultClient() : void
	{
		$this->assertInstanceOf(ClientInterface::class, $this->_object->createClient());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ClientFactory();
	}
	
}
